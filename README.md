# TpFinalmonzon

**TP Final**
_Fecha_: 08/03/2022
_Nombre y apellido_: Agustina Monzon Ramirez
_Materia_: Infraestructura de Servidores
_Profesor_: Sergio Pernas

**Trabajo Práctico**
_Docker file_

Crear una imagen Docker que puede contener:

- [ ] Una aplicación de desarrollo propio (dinamica o html estatico).

- [ ] Una aplicación de Docker Hub, la imagen original debe ser modificada para con
caracterisicas que no posea la original, por ejemplo, usar una imagen de Wordpress para
crear una imagen nueva de Wordpress pero que incluya plugins o nuevos themes.

- [ ] Implementar una aplicacion de terceros partiendo de una imagen base como por ejemplo
Ubuntu.

- [ ] Una imagen orientada a herramientas en el ambito de administracion de sistemas e
infraestructuras, por ejemplo, un servicio DNS, proxy, analizador de logs, etc.

_Repositorio GIT_

Crear un repositorio GIT para el proyecto a entregar. El repositorio debe contener:

- [ ] Dockerfile.

- [ ] Ficheros necesarios para construir la imagen.

- [ ] Incluir el fichero README.md

_README.md_

El README debe inlcuir:

- [ ] Explicación de cada aspecto volcado en el Dockerfile.

- [ ] Explicación de los scripts, si los hubiere.

- [ ] Explicación de ficheros de configuración, si los hubiere.

- [ ] Explicación de los pasos para construir la imagen.

- [ ] Pasos para hacer el deploy del contenedor en distintintas modalidades (con volumenes, con
variables de entorno, etc), si la imagen lo permite.


Configuracion entorno virtual

Creacion maquina virtual con VirtualBox utilizando una imagen base (Ubuntu) proporcionada en clase.

    Definimos un hostname   

_$ sudo hostnamectl set-hostname agusdocker_

Aplicamos el cambio reiniciando el equipo 

_$ sudo reboot_

    Definimos una IP

Descargamos el editor VIM para poder editar el archivo de red 01-netcfg.yaml

_$ sudo apt update &&  apt install vim -y_

Editamos el archivo con el siguiente comando. Desactivamos el dhcp e ingresamos una IP fija.

_$ sudo vim /etc/netplan/01-netcfg.yaml_

Aplicamos los cambios con el comando

_$ sudo netplan apply_

    Definimos Firewalls

Deshabilitamos todo el trafico entrante

_$ sudo ufw default deny incoming_

Habilitamos todo el trafico saliente

_$ sudo ufw default allow outgoing_

Habilitamos SSH

_$ sudo ufw allow ssh_

Activamos el Firewall

_$ sudo ufw enable_

Habilitamos 'http' 

_$ sudo ufw allow http_

Habilitamos 'https' 

_$ sudo ufw allow https_

    Instalacion Docker

_# apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y_

Llave publica

_# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add_

Repositorio oficial de Docker

_$ add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"_

_# apt update_

_$ apt install docker-ce docker-ce-cli containerd.io_

Agregamos nuestro usuario al grupo Docker asi evitamos usar sudo

_# gpasswd -a agus docker_ 

Podemos probar que todo funcione correctamente corriendo un contenedor de prueba

_$ docker run hello-world_

    Creacion imagen Docker

_$ mkdir dockerfile_

Vamos a crear el archivo de configuracion Dockerfile que nos sirve para automatizar los pasos para crear una imagen.

_$ vim Dockerfile_

Instrucción FROM ubuntu:latest indica una imagen base para crear una nueva imagen.

Instrucción LABEL version="1" pasa información acerca de la imagen.

Instrucción ARG DEBIAN_FRONTEND=noninteractive define variables de entorno dentro del nuevo contenedor para crear la imagen y le pasa el tipo de ejecución a los programas que lo soliciten

Instrucción RUN apt update && apt install -y nginx ejecuta un comando dentro del contenedor temporal.

Instrucción COPY default /etc/nginx/sites-available  copia ficheros al contenedor temporal.

Instrucción VOLUME ["/var/www","/etc/nginx"] habilita los directorios definidos del contenedor para ser montados con directorios externos.

Instrucción EXPOSE 80 443 expone los puertos del contenedor.

Instrucción CMD ["nginx", "-g", "daemon off;"] indica qué comando se ejecutará cuando el contenedor se inicie

    Creamos el fichero Default en el mismo directorio donde se encuentra el Dockerfile

_$ vim default_

Fichero 'default'

server {

 listen 80 default_server;

 root /var/www;

 index index.html index.htm index.nginx-debian.html;

 server_name _;

 location / {

  try_files $uri $uri/ =404;

 }
 
}

    Creamos el archivo index.html dentro de Dockerfile tambien. Este es nuestro contenido estatico que podremos ver por nuestro navegador Web.

_$ vim index.html_

    Creamos la Imagen

_$ docker build -t nginx-tpfinal2:latest . _

Nuestra imagen ya deberia estar creada. Lo podemos corroborar con el siguiente comando:

_$ docker images_

    Creamos un contenedor a partir de la Imagen

_$ docker run --name test-7 -p 8080:80 -d nginx-tpfinal2_

Listo los contenedores para corroborar que se creo correctamente

_$ docker ps_

    El ultimo paso es verificar el sito web en el navegador ingresando la IP del host y el puerto previamente definido

_http://192.168.0.2:8080/_

